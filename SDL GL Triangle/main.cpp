
#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

#include "rt3d.h"
#include "rt3dObjLoader.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <stack>
#include "md2model.h"
#include <time.h>
#include <SDL_ttf.h>
#include "Camera.h"
#include <iostream>
#include <cstdlib>
#include <sstream>
#include "particle.h"
using namespace std;

#define DEG_TO_RADIAN 0.017453293

// Globals
// Real programs don't use globals :-D
GLenum err=0;
GLuint meshIndexCount = 0;
GLuint md2VertCount = 0;
GLuint bunnyCount = 0;
GLuint dragonCount = 0;
GLuint meshObjects[3];
GLfloat cubeRot=0.0f;
GLuint shaderProgram;
GLuint skyboxProgram;
GLuint toonProgram;
GLuint particleProgram;
float totalTime = 0.0f;
int mode = 0;
GLfloat r = 0.0f;
float deltaTime = 0;
float currentTime = 0;
float lastTime = 0;
glm::vec3 eye(0.0f, 1.0f, 0.0f);
glm::vec3 at(0.0f, 1.0f, -1.0f);
glm::vec3 up(0.0f, 1.0f, 0.0f);
glm::vec3 attenuation(1.0f,0.01f,0.01f);
glm::mat4 envMapProjection;
stack<glm::mat4> mvStack; 
Camera* camera;
Camera* envMapCams[6];
GLint reflectMode;
float mspf;
// TEXTURE STUFF
GLuint textures[4];
GLuint skybox;
GLuint labels[7];
GLuint envCube;
GLuint cubeFBO;
GLuint currentMesh;
// the particle system itself
particle* particleSystem;
GLuint currentMeshCount;
GLenum sides[6] = { GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_Z,
					GL_TEXTURE_CUBE_MAP_POSITIVE_X,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
					GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
					GL_TEXTURE_CUBE_MAP_NEGATIVE_Y	};

rt3d::lightStruct light0 = {
	{0.3f, 0.3f, 0.3f, 1.0f}, // ambient
	{1.0f, 1.0f, 1.0f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{0.0f, 1.0f, 1.0f, 1.0f}  // position
};
//material with emissive properties
rt3d::materialStruct material0 = {
	{0.3f, 0.3f, 0.0f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.0f, 1.0f}, // diffuse
	{6.0f, 5.0f, 0.0f, 1.0f}, // specular
	{1.0f, 1.0f, 1.0f, 1.0f}, // emissive
	2.0f  // shininess
};
rt3d::materialStruct material1 = {
	{0.1f, 0.0f, 0.0f, 1.0f}, // ambient
	{0.5f, 0.0f, 0.0f, 1.0f}, // diffuse
	{1.0f, 0.0f, 0.0f, 1.0f}, // specular
	{0.0f, 0.0f, 0.0f, 0.0f}, // emissive
	1.0f  // shininess
};
rt3d::materialStruct material2 = {
	{0.6f, 0.5f, 0.5f, 1.0f}, // ambient
	{0.5f, 0.5f, 0.5f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{1.0f, 1.0f, 1.0f, 0.0f}, // emissive
	1.0f  // shininess
};
rt3d::materialStruct material3 = {
	{0.6f, 0.5f, 0.5f, 1.0f}, // ambient
	{0.1f, 0.5f, 0.6f, 1.0f}, // diffuse
	{1.0f, 1.0f, 1.0f, 1.0f}, // specular
	{1.0f, 1.0f, 1.0f, 0.0f}, // emissive
	1.0f  // shininess
};
glm::vec4 lightPos(-1.0f, 5.0f, 3.0f, 1.0f); //light position

// md2 stuff
md2model tmpModel;
int currentAnim = 0;

TTF_Font * textFont;
GLenum attachment[] = {GL_COLOR_ATTACHMENT0};
GLenum framebuff[] = {GL_BACK_LEFT};
// textToTexture
GLuint textToTexture(const char * str/*, TTF_Font *font, SDL_Color colour, GLuint &w,GLuint &h */) {
	TTF_Font *font = textFont;
	SDL_Color colour = { 255, 255, 255 };
	SDL_Color bg = { 0, 0, 0 };

	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(font,str,colour);

	if (stringImage == NULL)
		//exitFatalError("String surface not created.");
		std::cout << "String surface not created." << std::endl;

	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	if (colours == 4) {   // alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGBA;
	    else
		    format = GL_BGRA;
	} else {             // no alpha
		if (stringImage->format->Rmask == 0x000000ff)
			format = GL_RGB;
	    else
		    format = GL_BGR;
	}
	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	GLuint texture;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);
	return texture;
}


GLuint loadCubeMap(const char *fname[6], GLuint *texID) {
	glGenTextures(1, texID); // generate texture ID


	SDL_Surface *tmpSurface;
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); // bind tex & set params


	for (int i=0;i<6;i++) 
	{
		// load file - using core SDL library
		tmpSurface = SDL_LoadBMP(fname[i]);
		glTexImage2D(sides[i],0,GL_RGB,tmpSurface->w, tmpSurface->h,	 0, GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, 	GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, 	GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, 	GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, 	GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, 	GL_CLAMP_TO_EDGE);
		if (!tmpSurface) 
		{
			std::cout << "Error loading bitmap" << std::endl;
			return *texID;
		}
		// texture loaded, free the temporary buffer
		SDL_FreeSurface(tmpSurface);
	}
	return *texID;	// return value of texure ID
}
// Set up rendering context
SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        rt3d::exitFatalError("Unable to initialize SDL"); 
	  
    // Request an OpenGL 3.0 context.
	
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        rt3d::exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

// A simple texture loading function
// lots of room for improvement - and better error checking!
GLuint loadBitmap(char *fname) {
	GLuint texID;
	glGenTextures(1, &texID); // generate texture ID

	// load file - using core SDL library
 	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);
	if (!tmpSurface) {
		std::cout << "Error loading bitmap" << std::endl;
	}

	// bind texture and set parameters
	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	SDL_PixelFormat *format = tmpSurface->format;
	
	GLuint externalFormat, internalFormat;
	if (format->Amask) {
		internalFormat = GL_RGBA;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGBA : GL_BGRA;
	}
	else {
		internalFormat = GL_RGB;
		externalFormat = (format->Rmask < format-> Bmask) ? GL_RGB : GL_BGR;
	}

	glTexImage2D(GL_TEXTURE_2D,0,internalFormat,tmpSurface->w, tmpSurface->h, 0,
		externalFormat, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	glGenerateMipmap(GL_TEXTURE_2D);

	SDL_FreeSurface(tmpSurface); // texture loaded, free the temporary buffer
	return texID;	// return value of texture ID
}
void init(void) {
	// For this simple example we'll be using the most basic of shader programs
	toonProgram = rt3d::initShaders("phong-tex.vert","toon-notex.frag");
	//reflectProgram = rt3d::initShaders("displacement.vs.glsl","passthru.gs.glsl","envMap.fs.glsl");
	particleProgram = rt3d::initShaders("particle.vs.glsl","particle.fs.glsl");
	shaderProgram = rt3d::initShaders("phong-tex.vert","phong-tex.frag");
	skyboxProgram = rt3d::initShaders("skybox.vert","skybox.frag");
	//rt3d::setMaterial(shaderProgram, material0);
	camera = new Camera();
	camera->MoveForward(5.0f);
	camera->RotateYaw(180.0f);
	camera->MoveUp(2.0f);

	vector<GLfloat> verts;
	vector<GLfloat> norms;
	vector<GLfloat> tex_coords;
	vector<GLuint> indices;
	rt3d::loadObj("cube.obj", verts, norms, tex_coords, indices);

	GLuint size = indices.size();
	meshIndexCount = size;

	meshObjects[0] = rt3d::createMesh(verts.size()/3, verts.data(), nullptr, norms.data(), tex_coords.data(), size, indices.data());
	verts.clear();
	norms.clear();
	tex_coords.clear();
	indices.clear();

	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);


	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		cout << "TTF failed to initialise." << endl;

	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		cout << "Failed to open font." << endl;

	const char *cubeTexFiles[6] =
	{
		"Town-skybox/posz.bmp", 
		"Town-skybox/negz.bmp",  
		"Town-skybox/posx.bmp", 
		"Town-skybox/negx.bmp", 
		"Town-skybox/posy.bmp", 
		"Town-skybox/negy.bmp"
	};
	//load and set up skybox cubemap
	skybox=loadCubeMap(cubeTexFiles, &skybox);
	//load and set up environment cubemap and FBO for rendering
	textures[0] = loadBitmap("steelgrating.bmp");
	textures[1] = loadBitmap("studdedmetal.bmp");
	textures[2] = loadBitmap("smoke1.bmp");
	labels[0] = textToTexture(" Hello ");
	labels[2] = textToTexture("wasdrf move camera");
	labels[3] = textToTexture(",. rotate camera");
	labels[4] = textToTexture("arrows, o,p move Light");
	particleSystem = new particle(1024);
	particleSystem->init(16,1024/16,1.0f,glm::vec4(1.0,1.0,1.0,1.0),glm::vec4(1.0,0.0,0.0,0.0));
	//particleSystem->setRepeating(false);
	float velocityX = ((std::rand() % 100)/50.0f)-1.0f;
	float velocityY = 0.0;
	float velocityZ = ((std::rand() % 100)/50.0f)-1.0f;
	glm::vec4 force(velocityX,0.0f,velocityZ,0.0f);
	//particleSystem->applyForce(force);
	glEnable(GL_POINT_SPRITE);
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	glPointSize(5.0f);
	lastTime = clock();
}
glm::vec4 calculateViewSpaceLight(glm::mat4& viewMatrix)
{
	//////////////////////////////////////////////////////////////////////////////
	//CALCULATE VIEW TRANSFORMED LIGHT POSITION	PRIOR TO BEING PASSED TO SHADER	//
	//////////////////////////////////////////////////////////////////////////////
	glm::vec4 tmp = viewMatrix*lightPos;
	light0.position[0] = tmp.x;
	light0.position[1] = tmp.y;
	light0.position[2] = tmp.z;
	return glm::vec4 (tmp.x,tmp.y,tmp.z,1.0f);
}


void drawUI(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	////////////////////////////////////////////////////////////////////
	//BILL BOARDED TEXT BOX											////
	////////////////////////////////////////////////////////////////////

	//and activate basic shader for transparency, which we draw last
	glUseProgram(shaderProgram);
	// with text texture
	// THIS IS BILLBOARDED: SEE NO ROTATION
	//mvStack.push(mvStack.top());
	std::stack<glm::mat4> mStack;
	mStack.push(viewMatrix);
	mStack.push(mStack.top());
	glm::mat4 proj = glm::ortho<float>(-1.0f,1.0f,-1.0f,1.0f);
	glm::mat4 modelview(1.0);
	mStack.top() = glm::translate(mStack.top(), eye+glm::vec3(-0.75f, 0.9f, 0.0f));
	mStack.push(mStack.top());
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.15f,0.05f,0.1f));
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));
	glm::mat4 RotOnly = glm::mat4(glm::mat3(viewMatrix));
	//set uniforms
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(proj));
	rt3d::setUniformMatrix3fv(shaderProgram, "normalmatrix", glm::value_ptr(normalmatrix));

	rt3d::setLight(shaderProgram, light0);
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLightPos(shaderProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(shaderProgram, material0);
	//bind texture

	//render call
	glDepthMask(GL_FALSE);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,labels[1]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//back to translated matrix
	mStack.pop();
	//DRAW OTHER UI ELEMENTS HERE
	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.1f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.18f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[2]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();
		
	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.2f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.16f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[3]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();

	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.3f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.17f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[4]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();

	mStack.push(mStack.top());
	mStack.top() = glm::translate(mStack.top(), glm::vec3(0.0f, -0.4f, 0.0f));
	mStack.top() = glm::scale(mStack.top(),glm::vec3(0.2f,0.05f,0.0f));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(glm::inverse(RotOnly)*mStack.top()));
	glBindTexture(GL_TEXTURE_2D,labels[5]);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	mStack.pop();


	/////////////////////////////
	//back to viewMatrix
	mStack.pop();
	//empty stack
	mStack.pop();
	// remember to use at least one pop operation per push...
	glDepthMask(GL_TRUE);
	glUseProgram(0);
}
void drawSkyBox(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	////////////////////////////////////////////////////////////////////////////
	//DRAW SKY BOX															  //	
	////////////////////////////////////////////////////////////////////////////
	glUseProgram(skyboxProgram);
	glDepthMask(GL_FALSE); // update depth mask off
	//glm::mat3 mvRotOnlyMat3 = glm::mat3(mvStack.top());
	glCullFace(GL_FRONT); // drawing inside of cube
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);
	//calculate transform matrices
	glm::mat4 mView= glm::mat4(glm::mat3(viewMatrix));
	glm::mat4 modelview = mView;
	mView = glm::scale(modelview, 	glm::vec3(1.5f, 1.5f, 1.5f));
	//set shader uniforms
	rt3d::setUniformMatrix4fv(skyboxProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformMatrix4fv(skyboxProgram, "modelview", 	glm::value_ptr(mView));
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//pop matrix and reset culling and depth mask
	//mvStack.pop(); 
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	glCullFace(GL_BACK); glDepthMask(GL_TRUE); 
	glUseProgram(0);
	//end draw skybox
}
void drawGround(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	////////////////////////////////////////////////////////
	// FLATTENED CUBE TO REPRESENT GROUND				////
	////////////////////////////////////////////////////////
	glUseProgram(shaderProgram);
	//glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textures[0]);
	//mvStack.push(mvStack.top());
	glm::mat4 modelview(1.0f);
	modelview = glm::translate(modelview, glm::vec3(0.0f, -0.1f, 0.0f));
	modelview = glm::scale(modelview,glm::vec3(1.0f, 0.1f, 1.0f));
	modelview = viewMatrix * modelview;
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(modelview));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix3fv(shaderProgram,"normalmatrix", glm::value_ptr(normalmatrix));
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLight(shaderProgram, light0);

	rt3d::setLightPos(shaderProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(shaderProgram, material2);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//mvStack.pop();
}
void drawLight(glm::mat4& projectionMatrix, glm::mat4& viewMatrix)
{
	///////////////////////////////////////////////////////////////////////////////
	//GENERAL SHADER FOR RENDERING												///
	///////////////////////////////////////////////////////////////////////////////
	glUseProgram(shaderProgram);
	///////////////////////////////////////////////////////////////////////////////
	//DRAW LIGHT SOURCE VISUAL REPRESENTATION									///
	///////////////////////////////////////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, textures[0]);
//	mvStack.push(mvStack.top());

	glm::mat4 modelview(1.0);
	modelview = glm::translate(modelview, glm::vec3(lightPos));
	modelview = glm::scale(modelview,glm::vec3(0.25f));
	modelview = viewMatrix * modelview;
	glm::mat3 normalmatrix = glm::inverse(glm::transpose(glm::mat3(modelview)));
	rt3d::setUniformMatrix4fv(shaderProgram, "projection", glm::value_ptr(projectionMatrix));
	rt3d::setUniformMatrix4fv(shaderProgram, "modelview", glm::value_ptr(modelview));
	rt3d::setUniformVector3fv(shaderProgram, "attenuation", glm::value_ptr(attenuation));
	rt3d::setUniformMatrix3fv(shaderProgram,"normalmatrix", glm::value_ptr(normalmatrix));
	glm::vec4 viewLight = calculateViewSpaceLight(viewMatrix);
	rt3d::setLight(shaderProgram, light0);

	rt3d::setLightPos(shaderProgram, glm::value_ptr(viewLight));
	rt3d::setMaterial(shaderProgram, material0);
	rt3d::drawIndexedMesh(meshObjects[0],meshIndexCount,GL_TRIANGLES);
	//mvStack.pop();
}
// parse user input
void update(void) {

	currentTime = clock();
	deltaTime = currentTime - lastTime;
	mspf = deltaTime;
	stringstream ss;
	ss<<"ms/f : "<<deltaTime;
	totalTime += deltaTime/100000;
	glDeleteTextures(1,&labels[1]);
	labels[1] = textToTexture(ss.str().c_str());
	const Uint8 *keys = SDL_GetKeyboardState(NULL);
	//r is yaw angle, eye is location of camera, 0.1f is move speed (per key press: not ideal)
	if ( keys[SDL_SCANCODE_W] ) camera->MoveForward(0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_S] ) camera->MoveForward(-0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_A] ) camera->MoveRight(-0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_D] ) camera->MoveRight(0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_R] ) camera->MoveY(0.01f*deltaTime);
	if ( keys[SDL_SCANCODE_F] ) camera->MoveY(-0.01f*deltaTime);

	if ( keys[SDL_SCANCODE_UP] )  lightPos.z -=0.1f;
	if ( keys[SDL_SCANCODE_DOWN] ) lightPos.z += 0.1f;
	if ( keys[SDL_SCANCODE_LEFT] ) lightPos.x -= 0.1f;
	if ( keys[SDL_SCANCODE_RIGHT] ) lightPos.x += 0.1f;
	if ( keys[SDL_SCANCODE_O] ) lightPos.y -= 0.1;
	if ( keys[SDL_SCANCODE_P] ) lightPos.y += 0.1;

	if ( keys[SDL_SCANCODE_COMMA] ) camera->RotateYaw(0.1f*deltaTime);
	if ( keys[SDL_SCANCODE_PERIOD] ) camera->RotateYaw(-0.1f*deltaTime);


	cubeRot+=0.01f*deltaTime;
	lastTime = currentTime;
	particleSystem->update(deltaTime);
	//srand (time(NULL));
	float velocityX = ((std::rand() % 100)/50.0f)-1.0f;
	float velocityY = 0.0;
	//srand (time(NULL));

	float velocityZ = ((std::rand() % 100)/50.0f)-1.0f;
	glm::vec4 force(velocityX,0.0f,velocityZ,0.0f);
	particleSystem->applyForce(force);

}
void drawScene(glm::mat4& projectionMatrix, int pass)
{
	glm::mat4 viewMatrix(1.0);
	glEnable(GL_CULL_FACE);
	glClearColor(0.5f,0.5f,0.5f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);
	camera->getViewMatrix(viewMatrix, false);
	eye = camera->getPosition();
	glEnable(GL_CULL_FACE);
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glClear(GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT);

	//draw scene elements
	//drawSkyBox(projectionMatrix,viewMatrix);
	//drawGround(projectionMatrix,viewMatrix);
	//drawLight(projectionMatrix,viewMatrix);
	glUseProgram(particleProgram);
	glBindTexture(GL_TEXTURE_2D,textures[2]);
	glm::mat4 model(1.0f);
	model = glm::scale(model,glm::vec3(1.0f));
	rt3d::setUniformMatrix4fv(particleProgram, "modelview", glm::value_ptr(viewMatrix * model));
	rt3d::setUniformMatrix4fv(particleProgram, "projection", glm::value_ptr(projectionMatrix));
	particleSystem->draw();
	drawUI(projectionMatrix,viewMatrix);

}
void draw(SDL_Window * window) {
	// clear the screen
	glm::mat4 projection = glm::perspective(60.0f,800.0f/600.0f,1.0f,150.0f);
	GLfloat scale(1.0f); // just to allow easy scaling of complete scene
	

	drawScene(projection,0);
	//order of drawing is important
	//draw scene to FBO, except the reflective bunny, as the render target is the bunny cubemap
	//for each render target render scene to FBO texture


    SDL_GL_SwapWindow(window); // swap buffers
}
// Program entry point - SDL manages the actual WinMain entry point for us
int main(int argc, char *argv[]) {
    SDL_Window * hWindow; // window handle
    SDL_GLContext glContext; // OpenGL context handle
    hWindow = setupRC(glContext); // Create window and render context 
	// Required on Windows *only* init GLEW to access OpenGL beyond 1.1
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) { // glewInit failed, something is seriously wrong
		std::cout << "glewInit failed, aborting." << endl;
		exit (1);
	}
	cout << glGetString(GL_VERSION) << endl;

	init();

	bool running = true; // set running to true
	SDL_Event sdlEvent;  // variable to detect SDL events
	while (running)	{	// the event loop
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
		}
		update();
		draw(hWindow); // call the draw function
	}

    SDL_GL_DeleteContext(glContext);
    SDL_DestroyWindow(hWindow);
    SDL_Quit();
    return 0;
}