// Phong fragment shader phong-tex.frag matched with phong-tex.vert
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emissive;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform vec3 attenuation;
in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
layout(location = 0) out vec4 out_Color;
 
void main(void) {
	// Ambient intensity
	vec4 ambientI = vec4(vec3(0.0),1.0);
	//if(light.position.w>0)
	//{
		float dist = length(ex_L);
		float K = attenuation.x;
		float kD = attenuation.y*dist;
		float kDs = attenuation.z*(dist*dist);
		float atten = 1.0/(K+kD+kDs);
		//atten = 1.0/(1.0 + 0.1*dist + 0.1*dist*dist);
		// Diffuse intensity
		vec4 diffuseI = light.diffuse * material.diffuse;
		diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);
		// Specular intensity
		// Calculate R - reflection of light
		vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));
		vec4 specularI = light.specular * material.specular;
		specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);
			
		vec4 litColour = (ambientI + (diffuseI + specularI)*vec4(atten, atten, atten, 1.0));

		// seperate pixel colour out into reduced pallete
		// this is the cel shading part of the shader
		vec4 shade1 = smoothstep(vec4(0.15),vec4(0.2),litColour);
		vec4 shade2 = smoothstep(vec4(0.4),vec4(0.6),litColour);
		vec4 shade3 = smoothstep(vec4(0.75),vec4(1.0),litColour);

		vec4 colour = max( max(0.3*shade1,0.6*shade2), shade3 );
		//draws a black pixel anywhere we can see the edges of a mesh, with the normal perpendicular to view vector
		if ( abs(dot(ex_V,ex_N)) < 0.2)
			colour = vec4(vec3(0.0),1.0);
		////final colour of pixel
		out_Color = vec4(colour.rgb,1.0);
		//out_Color = litColour;
}