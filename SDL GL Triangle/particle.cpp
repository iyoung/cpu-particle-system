#include "particle.h"
#include <time.h>
#include <math.h>

particle::particle(void)
{
	m_MaxNumParticles = 100;
	m_EmitterPosition = glm::vec4(glm::vec3(0.0f),1.0f);

}
particle::particle(const unsigned int& p_MaxNumParticles)
{
	m_MaxNumParticles = p_MaxNumParticles;
	m_EmitterPosition = glm::vec4(0.0f,1.1f,0.0f,1.0f);
}
bool particle::init(	const int &WavesPerSecond,
						const int &ParticlesPerWave,
						const float &maxParticleLife,
						const glm::vec4 &startColour,
						const glm::vec4 &endColour	)
{
	running = true;
	m_StartColour = startColour;
	m_NumWaves = WavesPerSecond;
	m_ParticlePositions.resize(m_MaxNumParticles,m_EmitterPosition);
	m_ParticleColours.resize(m_MaxNumParticles,startColour);
	m_DeltaColour=endColour-startColour;
	m_DeltaColour*=1.0/maxParticleLife;
	m_ParticleLifeTimes.resize(WavesPerSecond,maxParticleLife);
	m_ParticlesPerEmission = ParticlesPerWave;
	m_MaxParticleLifetime = maxParticleLife;
	m_WaveDelays.resize(WavesPerSecond,1.0f/WavesPerSecond);
	m_WaveTimers.resize(WavesPerSecond,0.0f);
	for (std::size_t i = 0; i < WavesPerSecond;i++)
	{
		m_WaveDelays[i]*=i;
		//m_WaveTimers[i]*=i;
	}
	m_ParticleVelocities.resize(m_MaxNumParticles);
	for (std::size_t position = 0; position < m_MaxNumParticles; position ++)
	{
		float azimuth = (std::rand() % 360)-1.0f;
		float altitude = (std::rand() % 360)-1.0f;
		float hyp = cos(glm::radians(altitude));
		float velocityX = hyp*glm::radians(sin(azimuth));
		float velocityZ = hyp*glm::radians(cos(azimuth));
		float velocityY = glm::radians(sin(altitude));
		glm::vec4 v(velocityX,velocityY, velocityZ, 0.0f);
		m_ParticleVelocities[position] = glm::normalize(v);
		m_ParticleVelocities[position].y=0.0f;
	}
	m_ParticleBuffers = new GLuint[NUM_PARTICLE_BUFFERS];
	//create and bind a Vertex array object to store our particles on the GPU
	glGenVertexArrays(1, &m_ParticleHandle);
	glBindVertexArray(m_ParticleHandle);
	//now we populate the VAO with Vertex Buffer data, just as if we were creating a mesh, except we are using points.
	glGenBuffers(1, &m_ParticleBuffers[PARTICLE_POSITION]);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleBuffers[PARTICLE_POSITION]);
	//as this data can change dynamically, we specify this to opengl. Unfortunately, storage size cannot be dynamic, 
	//so we create the storage at maximum size, regardless of how many particles we actually have.
	glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(glm::vec4), m_ParticlePositions.data(), GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)PARTICLE_POSITION, 4, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(PARTICLE_POSITION);
	//not needed
	//glGenBuffers(1, &m_ParticleBuffers[PARTICLE_VELOCITY]);
	//glBindBuffer(GL_ARRAY_BUFFER, m_ParticleBuffers[PARTICLE_VELOCITY]);
	////as this data can change dynamically, we specify this to opengl. Unfortunately, storage size cannot be dynamic, 
	////so we create the storage at maximum size, regardless of how many particles we actually have.
	//glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(glm::vec4), m_ParticleVelocities.data(), GL_DYNAMIC_DRAW);
	//glVertexAttribPointer((GLuint)PARTICLE_VELOCITY, 4, GL_FLOAT, GL_FALSE, 0, 0); 
	//glEnableVertexAttribArray(PARTICLE_VELOCITY);

	glGenBuffers(1, &m_ParticleBuffers[PARTICLE_COLOUR]);
	glBindBuffer(GL_ARRAY_BUFFER, m_ParticleBuffers[PARTICLE_COLOUR]);
	//as this data can change dynamically, we specify this to opengl. Unfortunately, storage size cannot be dynamic, 
	//so we create the storage at maximum size, regardless of how many particles we actually have.
	glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(glm::vec4), m_ParticleColours.data(), GL_DYNAMIC_DRAW);
	glVertexAttribPointer((GLuint)PARTICLE_COLOUR, 4, GL_FLOAT, GL_FALSE, 0, 0); 
	glEnableVertexAttribArray(PARTICLE_COLOUR);
	//not needed
	//this VBO is used for a simple compute shader, or for cpu processing. undecided yet
	//glGenBuffers(1, &m_ParticleBuffers[PARTICLE_TTL]);
	//glBindBuffer(GL_ARRAY_BUFFER, m_ParticleBuffers[PARTICLE_TTL]);
	////as this data can change dynamically, we specify this to opengl. Unfortunately, storage size cannot be dynamic, 
	////so we create the storage at maximum size, regardless of how many particles we actually have.
	//glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(GLfloat), m_ParticleLifeTimes.data(), GL_DYNAMIC_DRAW);
	//glVertexAttribPointer((GLuint)PARTICLE_TTL, 1, GL_FLOAT, GL_FALSE, 0, 0); 
	//glEnableVertexAttribArray(PARTICLE_TTL);
	//now we are finished creating space on the GPU, so we can unbind the VBO and VAO
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return true;
}
void particle::updateWave(std::size_t start, std::size_t end, std::size_t waveNum, float deltaTime)
{
	//for each particle in particles
	for (std::size_t position = start; position <= end; position++)
	{

		m_ParticlePositions[position] += m_ParticleVelocities[position]*deltaTime;
		//forces can be added here
		m_ParticleColours[position] += m_DeltaColour * deltaTime;
	}
}
void particle::resetWave(std::size_t start, std::size_t end, std::size_t waveNum)
{

	for (std::size_t position = start; position <= end && position<m_MaxNumParticles; position++)
	{
		//reset positions, velocity, colour and lifetime
		m_ParticlePositions[position] = m_EmitterPosition;
		float azimuth = (std::rand() % 360)-1.0f;
		float altitude = (std::rand() % 360)-1.0f;
		float hyp = cos(glm::radians(altitude));
		float velocityX = hyp*glm::radians(sin(azimuth));
		float velocityZ = hyp*glm::radians(cos(azimuth));
		float velocityY = glm::radians(sin(altitude));
		glm::vec4 v(velocityX,velocityY, velocityZ, 0.0f);
		m_ParticleVelocities[position] = glm::normalize(v);
		m_ParticleVelocities[position].y=0.0f;
		m_ParticleColours[position] = m_StartColour;
	}
}
void particle::update(const float& p_DeltaTime)
{
	
	float deltaTime = 	p_DeltaTime/1000.0f;
	//update all waves
	for (std::size_t waveNum = 0; waveNum < m_NumWaves; waveNum++)
	{
		//if wave timer has elapsed
		float timer = m_WaveTimers[waveNum];
		float delay = m_WaveDelays[waveNum];
		if(m_WaveTimers[waveNum] >= m_WaveDelays[waveNum])
		{
			//if wave has expired and this is a repeating pattern, reset the wave
			std::size_t start = m_ParticlesPerEmission*waveNum;
			std::size_t end = start+m_ParticlesPerEmission-1;
			if(m_ParticleLifeTimes[waveNum] < 0.0f)
			{
				if(repeating)
				{
					resetWave(start,end,waveNum);
					m_ParticleLifeTimes[waveNum] = m_MaxParticleLifetime;
				}
			}
			else
			{
				//else update the wave
				updateWave(start,end,waveNum,deltaTime);
				m_ParticleLifeTimes[waveNum] -= deltaTime;
			}
		}
		else
		{
			//else update the wave timer
			m_WaveTimers[waveNum] += deltaTime;
		}
	}

	//now update gpu stored data. Even though we don't need most of this data, we will eventually move all calculations
	//to the gpu
	glBindBuffer(GL_ARRAY_BUFFER,m_ParticleBuffers[PARTICLE_POSITION]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(GLfloat)*4, m_ParticlePositions.data(), GL_DYNAMIC_DRAW);
	//not needed
	//glBindBuffer(GL_ARRAY_BUFFER,m_ParticleBuffers[PARTICLE_VELOCITY]);
	//glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(GLfloat), m_ParticleVelocities.data(), GL_DYNAMIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER,m_ParticleBuffers[PARTICLE_COLOUR]);
	glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(GLfloat)*4, m_ParticleColours.data(), GL_DYNAMIC_DRAW);
	//not needed
	//glBindBuffer(GL_ARRAY_BUFFER, m_ParticleBuffers[PARTICLE_TTL]);
	//glBufferData(GL_ARRAY_BUFFER, m_MaxNumParticles*sizeof(GLfloat), m_ParticleLifeTimes.data(), GL_DYNAMIC_DRAW);
	glBindVertexArray(0);
}
void particle::draw()
{
	glDepthMask(0);
	glEnable(GL_BLEND);
	glBindVertexArray(m_ParticleHandle);	// Bind mesh VAO
	glDrawArrays(GL_POINTS, 0, m_MaxNumParticles);	// draw first vertex array object
	glBindVertexArray(0);
	glDisable(GL_BLEND);
	glDepthMask(1);
}
void particle::applyForce(const glm::vec4& p_Force)
{
	m_ExternalForce+=p_Force;
}
void particle::addAttractor(const glm::vec4& p_AttractorLocation, const float& p_AttractorStrength)
{
	m_Attractors.push_back(p_AttractorLocation);
	m_AttractorStrengths.push_back(p_AttractorStrength);
}
particle::~particle(void)
{
	m_ParticlePositions.clear();
	m_ParticleColours.clear();
	m_ParticleVelocities.clear();
	m_ParticleLifeTimes.clear();
	glDeleteBuffers(4,m_ParticleBuffers);
	delete[] m_ParticleBuffers;
}
