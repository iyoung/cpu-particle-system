
//phong environment mapping reflection pixel shader
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emissive;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform samplerCube textureUnit1;
uniform vec3 attenuation;
uniform float refractIndex;
//0 for reflection only, no lighting, 1 for reflection only with lighting, 2 for refraction only, 3 for all effects
uniform int reflectionMode;
in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec2 ex_TexCoord;
in vec3 ex_WorldNormal;
in vec3 ex_WorldView;
layout(location = 0) out vec4 out_Color;
 
void main(void) {
	//calculate data needed for Fresnel effect, and chromatic abberation
	const float EtaR = 1.0;
	const float EtaG = 1.1; // Ratio of indices of refraction
	const float EtaB = 1.2;
	//calculate the reflected world view vector
	vec3 tmpReflect = vec4(reflect(-ex_WorldView,normalize(ex_WorldNormal)),1.0).xyz;
	//calculate refracted vector for each colour channel
	vec4 tmpRefractR = vec4(refract(-ex_WorldView,normalize(ex_WorldNormal),EtaR/refractIndex),1.0);
	vec4 tmpRefractG = vec4(refract(-ex_WorldView,normalize(ex_WorldNormal),EtaG/refractIndex),1.0);
	vec4 tmpRefractB= vec4(refract(-ex_WorldView,normalize(ex_WorldNormal),EtaB/refractIndex),1.0);

	//calculate attenuation of light
	float dist = length(ex_L);
	float K = attenuation.x;
	float kD = attenuation.y*dist;
	float kDs = attenuation.z*(dist*dist);
	float atten = 1.0/(K+kD+kDs);
	// Ambient intensity
	vec4 ambientI = vec4(vec3(0.0),1.0);
	// Diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse;
	diffuseI = diffuseI * max(dot(normalize(ex_N),normalize(ex_L)),0);
	// Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(reflect(normalize(-ex_L),normalize(ex_N)));
	vec4 specularI = light.specular * material.specular;
	specularI = specularI * pow(max(dot(R,ex_V),0), material.shininess);
	//get pixel colour from 2d texture
	vec4 texColor = texture2D(textureUnit0, ex_TexCoord);
	//get pixel colours from cube map
	vec4 cubeColour = textureCube(textureUnit1,tmpReflect);
	vec4 refractColor;
	//sample and assign seperately for each channel
	refractColor.r = textureCube(textureUnit1,tmpRefractR).r;
	refractColor.g = textureCube(textureUnit1,tmpRefractG).g;
	refractColor.b = textureCube(textureUnit1,tmpRefractB).b;

	//reflection only
	if(reflectionMode==0)
	{
		out_Color = cubeColour * material.ambient;
	}
	//no reflection
	if(reflectionMode==1)
	{
		out_Color = (material.ambient *  (ambientI + (diffuseI + specularI)*vec4(atten, atten, atten, 1.0)));
	}
	//refraction only: lensing mode
	if(reflectionMode==2)
	{
		out_Color =  vec4(refractColor.xyz,1.0);
	}
	//refraction and view dependant reflection
	if(reflectionMode==3)
	{
		out_Color =  vec4(mix(cubeColour.rgb,refractColor.rgb,dot(ex_V,ex_N)),1.0);
	}	
}