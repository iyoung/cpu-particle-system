//phong environment mapping reflection pixel shader
#version 330

// Some drivers require the following
precision highp float;

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emissive;
	float shininess;
};

in GS_FS_INTERFACE
{
	in vec3 worldnormal;
	in vec2 texCoord;
	in vec3 WorldView;
	in vec3 lightvec;
	in vec3 viewNormal;
	in vec3 view;
} fs_in;

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;
uniform samplerCube textureUnit1;
uniform vec3 attenuation;
//uniform mat4 reflectionmatrix;
//uniform float refractIndex;
//0 for reflection only, no lighting, 1 for reflection only with lighting, 2 for refraction only, 3 for all effects

layout(location = 0) out vec4 out_Color;
 
void main(void) {

		const float EtaR = 0.69;
		const float EtaG = 0.695; // Ratio of indices of refraction
		const float EtaB = 0.7;
		// Ambient intensity
		vec4 ambientI = vec4(vec3(0.0),1.0);
		vec3 hVec = normalize(fs_in.lightvec+fs_in.view);
		float dist = length(fs_in.lightvec);
		float K = attenuation.x;
		float kD = attenuation.y*dist;
		float kDs = attenuation.z*(dist*dist);
		float atten = 1.0/(K+kD+kDs);
		// Diffuse intensity
		vec4 diffuseI = light.diffuse * material.diffuse;
		diffuseI = diffuseI * max(dot(normalize(fs_in.viewNormal),normalize(-fs_in.lightvec)),0.0);
		// Specular intensity
		// Calculate R - reflection of light
		vec3 R = normalize(reflect(normalize(-fs_in.lightvec),fs_in.viewNormal));
		vec4 specularI = light.specular * material.specular;
		specularI = specularI * pow(max(dot(R,normalize(fs_in.view)),0.0), material.shininess);
		//get pixel colour from 2d texture
		vec3 texColor = texture(textureUnit0, fs_in.texCoord.st).rgb;
		//get pixel colours from cube map
		//out_Color = vec4(1.0) * (ambientI + (diffuseI * specularI)*vec4(atten,atten,atten,1.0));
		//vec3 reflectView = vec4(reflect(-fs_in.WorldView,normalize(fs_in.worldnormal)),1.0).xyz;
		//vec3 reflectColour = texture(textureUnit1,reflectView).rgb;
		//+ (ambientI + diffuseI + specularI)
		vec3 tmpReflect = vec4(reflect(-fs_in.WorldView,normalize(fs_in.worldnormal)),1.0).xyz;
		//calculate refracted vector for each colour channel
		vec4 tmpRefractR = vec4(refract(-fs_in.WorldView,normalize(fs_in.worldnormal),EtaR/1.0),1.0);
		vec4 tmpRefractG = vec4(refract(-fs_in.WorldView,normalize(fs_in.worldnormal),EtaG/1.0),1.0);
		vec4 tmpRefractB= vec4(refract(-fs_in.WorldView,normalize(fs_in.worldnormal),EtaB/1.0),1.0);
		vec4 cubeColour = textureCube(textureUnit1,tmpReflect);
		vec4 refractColor;
		//sample and assign seperately for each channel
		refractColor.r = textureCube(textureUnit1,tmpRefractR).r;
		refractColor.g = textureCube(textureUnit1,tmpRefractG).g;
		refractColor.b = textureCube(textureUnit1,tmpRefractB).b;		
		//out_Color =  vec4(mix(cubeColour.rgb,refractColor.rgb,dot(fs_in.WorldView,fs_in.worldnormal)),1.0);
		//out_Color =  vec4(refractColor.rgb*cubeColour.rgb,1.0);
		out_Color = vec4((cubeColour.rgb + ambientI.rgb),1.0);
		//full lighting
		//out_Color = (material.ambient *  (ambientI + (diffuseI + specularI)*vec4(atten, atten, atten, 1.0)));
		//ambient+diffuse lighting
		//out_Color = (material.ambient *  (ambientI + (specularI)*vec4(atten, atten, atten, 1.0)));

}
