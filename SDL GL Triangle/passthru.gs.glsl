#version 330
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

uniform mat4 modelmatrix;
uniform mat4 viewmatrix;
uniform mat4 projection;
//inverse transpose modelview
uniform mat3 normalmatrix;
//world space light position
uniform vec4 lightPosition;
//lighting calculations use world space for this shader set
//which coincides with reflection requirements
uniform vec3 eyePosWorld;

out int gl_PrimitiveID;
//per-vertex inputs from vertex shader stage
in VS_GS_INTERFACE
{
	in vec3 position;
	in vec3 normal;
	in vec2 texCoord;
	in float displacement;
} gs_in[];

//per-vertex outputs to fragment shader
out GS_FS_INTERFACE
{
	out vec3 worldnormal;
	out vec2 texCoord;
	out vec3 WorldView;
	out vec3 lightvec;
	out vec3 viewNormal;
	out vec3 view;
} gs_out;

void main()
{
	// displace & transform vertices for triangle
	vec4 v1 = vec4(gs_in[0].position,0.0);
	v1 += vec4((gs_in[0].normal * gs_in[0].displacement), 1.0);

	vec4 v2 = vec4(gs_in[1].position,0.0);
	v2 += vec4((gs_in[1].normal * gs_in[1].displacement), 1.0);

	vec4 v3 = vec4(gs_in[2].position,0.0);
	v3 += vec4((gs_in[2].normal * gs_in[2].displacement), 1.0);
	//vec3 faceNormal = normalize(gs_in[0].normal+gs_in[1].normal+gs_in[2].normal);

	//NOTE: We can implement our own backface culling here, by inputing the view vector, and checking against the vertex normal

	/////////////vertex 1///////////////////
	//calculate 1st unweighted vertex normal
	vec4 ab = v2 - v1;
	vec4 ac = v3 - v1;
	vec3 tmpNorm = normalize(cross(ab.xyz,ac.xyz));
	//weight normal by it's original vertex normal
	vec3 faceNormal = normalize(gs_in[0].normal);

	tmpNorm = normalize(tmpNorm + faceNormal);
	gs_out.worldnormal = mat3(modelmatrix)*tmpNorm;
	gs_out.lightvec = (viewmatrix* (lightPosition-(modelmatrix * v1))).xyz;

	gs_out.viewNormal = normalmatrix * tmpNorm;
	//set remaining outputs	
	gl_Position = projection * viewmatrix * modelmatrix * v1;
	gs_out.texCoord = gs_in[0].texCoord;
	gs_out.view = (-(viewmatrix*modelmatrix * v1)).xyz;
	gs_out.WorldView = normalize(eyePosWorld - (modelmatrix*v1).xyz);

	//gs_out.EyeView = 
	//output vertex + additional data to rasteriser
	gl_PrimitiveID = gl_PrimitiveIDIn;
	EmitVertex();

	/////////////////vertex2/////////////////
	//calculate 2nd unweighted vertex normal
	ab = v3 - v2;
	ac = v1 - v2;
	tmpNorm = normalize(cross(ab.xyz,ac.xyz));
	//weight normal by it's original vertex normal
	faceNormal = normalize(gs_in[1].normal);

	tmpNorm = normalize(tmpNorm +faceNormal);

	gs_out.worldnormal = mat3(modelmatrix)*tmpNorm;
	gs_out.lightvec = (viewmatrix* (lightPosition-(modelmatrix * v2))).xyz;

	gs_out.viewNormal = normalmatrix * tmpNorm;

	//set remaining outputs
	gl_Position = projection * viewmatrix * modelmatrix * v2;
	gs_out.texCoord = gs_in[1].texCoord;
	gs_out.WorldView = normalize(eyePosWorld - (modelmatrix*v2).xyz);
	gs_out.view = (-(viewmatrix*modelmatrix * v2)).xyz;
	//output vertex + additional data to rasteriser
	gl_PrimitiveID = gl_PrimitiveIDIn;
	EmitVertex();

	/////////////////vertex 3//////////////////////
	//calculate 3rd unweighted vertex normal
	ab = v1 - v3;
	ac = v2 - v3;
	tmpNorm = normalize(cross(ab.xyz,ac.xyz));
	//weight normal by it's original vertex normal
	faceNormal = normalize(gs_in[2].normal);

	tmpNorm = normalize(tmpNorm +faceNormal);
	gs_out.worldnormal = mat3(modelmatrix)*tmpNorm;
	gs_out.lightvec = (viewmatrix* (lightPosition-(modelmatrix * v3))).xyz;
	gs_out.viewNormal = normalmatrix * tmpNorm;
	//set remaining outputs
	gl_Position = projection * viewmatrix * modelmatrix * v3;
	gs_out.texCoord = gs_in[2].texCoord;
	gs_out.WorldView = normalize(eyePosWorld - (modelmatrix*v3).xyz);
	gs_out.view = (-(viewmatrix*modelmatrix * v3)).xyz;

	//output vertex + additional data to rasteriser
	gl_PrimitiveID = gl_PrimitiveIDIn;
	EmitVertex();

	// geometry stage complete for this triangle
	EndPrimitive();
}
