#version 330
//vertex particle shader

layout (location = 0) in vec4 in_Position;
layout (location = 2) in vec4 in_Colour;

uniform mat4 modelview;
uniform mat4 projection;

out VS_FS_INTERFACE
{
	out vec4 position;
	out vec4 colour;
} vs_out;

void main()
{
	vs_out.position = projection * modelview * in_Position;
	vs_out.colour=in_Colour;
	gl_Position = projection * modelview * in_Position;
}