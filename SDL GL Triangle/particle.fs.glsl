#version 330
//fragment particle shader

in VS_FS_INTERFACE
{
	in vec4 in_position;
	in vec4 in_colour;
} fs_in;

layout (location = 0) out vec4 out_Colour;
uniform sampler2D textureUnit0;
uniform sampler2D textureUnit1;
void main()
{
	if (fs_in.in_colour.a > 0.0001)
	{
		out_Colour = fs_in.in_colour.rgba*texture(textureUnit0,gl_PointCoord);
	} 
	else
		discard;
}