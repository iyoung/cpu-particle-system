#pragma once
#include <glm\glm.hpp>
#include <vector>
#include <glew.h>
enum ParticleBufferSet
{
	PARTICLE_POSITION,
	PARTICLE_VELOCITY,
	PARTICLE_COLOUR,
	PARTICLE_TTL,
	NUM_PARTICLE_BUFFERS
};
class particle
{
public:
	particle(void);
	particle(const unsigned int& p_MaxNumParticles);
	bool init(	const int &WavesPerSecond,
				const int &ParticlesPerWave, 
				const float &maxParticleLife,
				const glm::vec4 &startColour,
				const glm::vec4 &endColour	);
	void update(const float& p_DeltaTime);
	void applyForce(const glm::vec4& p_Force);
	void addAttractor(const glm::vec4& p_AttractorLocation, const float& p_AttractorStrength);
	const unsigned int& getParticleHandle(){return m_ParticleHandle;}
	const unsigned int& getNumParticles();
	void draw();
	void setRepeating(bool pSwitch){repeating = pSwitch;}
	~particle(void);
protected:
	void updateWave(std::size_t start, std::size_t end, std::size_t waveNum, float deltaTime);
	void resetWave(std::size_t start, std::size_t end, std::size_t waveNum);
	std::vector<glm::vec4> m_ParticlePositions; //position of particles in 3d space
	std::vector<glm::vec4> m_ParticleVelocities; //direction of movement for particles
	std::vector<glm::vec4> m_ParticleColours; //includes alpha
	std::vector<float> m_ParticleLifeTimes; //current lifetime countdown for particles
	std::vector<float> m_WaveTimers;
	std::vector<float> m_WaveDelays;
	float m_MaxParticleLifetime; //maximum time particles can exist
	GLuint m_MaxNumParticles; //maximum number of particles.
	GLuint m_NumParticles; //current number of particles at an arbitrary time.
	GLuint m_ParticleHandle; //vao handle for particles (used by shader programs)
	glm::vec4 m_EmitterPosition; //origin point for particles
	float m_ParticleDelay; //delay for creation/reset of particles
	float m_ParticleDelayTimer;
	GLuint* m_ParticleBuffers; //used to access and update
	unsigned int m_ParticlesPerEmission;
	unsigned int m_MaxParticlesPerEmission;
	unsigned int m_NumWaves;
	glm::vec4 m_ExternalForce,
			  m_DeltaColour,
			  m_StartColour;
	std::vector<glm::vec4> m_Attractors;
	std::vector<float> m_AttractorStrengths;
	bool repeating,
		 running;
};

